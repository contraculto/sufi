<?php

$user = [
    "domain" => "contraculto.com",
    "username" => "rodrigo",
    "url" => "https://contraculto.com.com/ap/"
];

$request = $_SERVER['REQUEST_URI'];
if ( strpos($request, 'well-known/webfinger') ) {
    $request = '/.well-known/webfinger';
}
switch ($request) {

    case '/ap/actor' :
        require __DIR__ . '/actor.php';
        $content = actor();
        header('Content-Type: application/json; charset=utf-8');
        echo $content;
        break;

    case '/.well-known/webfinger':
        require __DIR__ . '/webfinger.php';
        $content = webfinger();
        header('Content-Type: application/json; charset=utf-8');
        echo $content;
        break;

    case '/ap/follow' :
        require __DIR__ . '/follow.php';
        break;

    case '/ap/outbox' :
        require __DIR__ . '/outbox.php';
        break;

    case '/ap/inbox' :
        require __DIR__ . '/inbox.php';
        break;

    case '/':
        require __DIR__ . '/home.php';
        break;

    default:
        http_response_code(404);
        echo "Not found :(\n";
        echo $request;
        break;
}

/*
    Return serialized JSON
<?php
$data = /** whatever you're serializing ** /;
header('Content-Type: application/json; charset=utf-8');
echo json_encode($data);
*/