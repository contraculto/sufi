<?php

// Webfinger CONTRACULTO.COM

function webfinger() {
	$data = [
		"subject" => "acct:rodrigo@contraculto.com",
		"aliases" => [
			"https://contraculto.com/@contraculto",
			"https://contraculto.com/@rodrigo",
			"https://contraculto.com/users/contraculto"
		  ],
		  "links" => [
			[
				"rel" => "http://webfinger.net/rel/profile-page",
				"type" => "text/html",
				"href" => "https://contraculto.com"
			],
			  [
				"rel" => "self",
				"type" => "application/activity+json",
				"href" => "https://contraculto.com"
			  ],
			[
			  "rel" => "http://ostatus.org/schema/1.0/subscribe",
			  "template" => "https://contraculto.com/ap/follow?uri={uri}"
			]
		]
	];

	return json_encode($data);
}

//print(webfinger());
?>
