<?php

// Actor CONTRACULTO.COM

function actor() {
	$data = [
		"@context" => [
			"https://www.w3.org/ns/activitystreams",
			"https://w3id.org/security/v1"
		],
		"id" => "https://contraculto.com.com/ap/actor",
		"type" => "Person",
		"preferredUsername" => "Contraculto",
		"name" => "Rod",
		"summary" => "A person in a forest",
		"inbox" => "https://contraculto.com/ap/inbox",
		"outbox" => "https://contraculto.com/ap/outbox/",
		"followers" => "https://contraculto.com/ap/followers/",
		"following" => "https://contraculto.com/ap/following/",
		"likes" => "https://contraculto.com/ap/likes/",
		"publicKey" => [
			"id" => "https://contraculto.com.com/ap/actor#main-key",
			"owner" => "https://contraculto.com/ap/actor",
			"publicKeyPem" => "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnyhc9Uwsa2aEYlEdnLoiAurG/7ajVPzmilJ7JHOPGt2P7NLzHwCHudd1pYuMx230NakHnWc+3YToSxA2gVc/qoDidAvWUvjB6BQCLypHToBZpCnVjQ4bXfpwEMcQKtFHHYN75tRvsS8vI65h7fMwhABRZBJDCN+vUm8+Atzc5Mg5+s+pgBP/m0pdLwd7Xzga99/7OyrF29yK5XJB4r/N97lcSIjQ0xTr8AqLpTnKmlaCsb5p6QyT4W2bR1ghdiosopXKdVSpu40KDpZEYQhDb1/ILqiTfGQJ2EIoIyNkEm3ZgUQJzjjsCERLQLjDPnt7xvIEG7CNf9meyU1J/rtFXQIDAQAB-----END PUBLIC KEY-----"
		]
	];

	return json_encode($data);
}

//print(actor());
?>
